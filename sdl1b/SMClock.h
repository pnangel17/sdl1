
//
// SMClock.h
// Copyright (c) 2015 Paul Angel.  This software is released under the MIT Licence (MIT).  Refer to mit_licence.txt in the DXFoundation directory.
//

// Model a clock to track frames-per-second and seconds-per-frame for the host application

#pragma once


#include <cstdint>
#include <string>
#include <memory>


typedef long long gu_time_index;
typedef long long gu_time_interval;
typedef double gu_seconds;


#pragma region GUClock cbuffer structures

// Structure to model the current frame game time delta - this is used to control GPU-based animations
struct GameTimeDeltaStruct {

	float						gameTimeDelta = 0.0f;
};

#pragma endregion


class SMClock_actual {

	// Private class to track frames-per-second (which varies non-linearly) and its inverse, seconds-per-frame
	class GUFrameCounter {

	private:

		int					frame;
		gu_seconds			fpsRefTimeIndex;
		unsigned long		fpsCounts;
		double				framesPerSecond, minimumFPS, maximumFPS, averageFPS;
		gu_seconds			secondsPerFrame, minimumSPF, maximumSPF, averageSPF;


	public:

		GUFrameCounter(gu_time_index baseTime = 0) {

			resetCounter(baseTime);
		}

		void resetCounter(gu_time_index resetTime = 0) {

			frame = 0;

			fpsRefTimeIndex = 0.0;

			fpsCounts = 0;

			framesPerSecond = minimumFPS = maximumFPS = averageFPS = 0.0;
			secondsPerFrame = minimumSPF = maximumSPF = averageSPF = 0.0;
		}


		void updateFrameCounterForElaspsedTime(gu_seconds gameTimeElapsed) {

			frame++;

			gu_seconds time_delta = gameTimeElapsed - fpsRefTimeIndex;

			if (time_delta >= 1.0) {

				framesPerSecond = (double)frame / time_delta;
				secondsPerFrame = time_delta / (double)frame;

				if (fpsCounts == 0) {

					// First iteration so initialise maximum, minimum and average fps and seconds per frame
					minimumFPS = maximumFPS = averageFPS = framesPerSecond;
					minimumSPF = maximumSPF = averageSPF = secondsPerFrame;

				}
				else {

					// Update maximum, minimum and average fps
					if (framesPerSecond < minimumFPS)
						minimumFPS = framesPerSecond;
					else if (framesPerSecond > maximumFPS)
						maximumFPS = framesPerSecond;

					averageFPS += framesPerSecond;

					// Update maximum, minimum and averse (milli)seconds per frame
					if (secondsPerFrame < minimumSPF)
						minimumSPF = secondsPerFrame;
					else if (secondsPerFrame > maximumSPF)
						maximumSPF = secondsPerFrame;

					averageSPF += secondsPerFrame;
				}

				// Reset frame counter for next iteration
				frame = 0;

				// Note:  if a process takes significantly longer than 1 second, incrementing ref time index by 1.0 second means it lags behind the actual game time index so the next update call will track a low number of FPS (perhaps just 1).  So fpsRefTimeIndex resets to the actual game time so we start tracking frames from this point.  This means the clock will be more precise for time complex operations (not likely in a game environment though)
				fpsRefTimeIndex = gameTimeElapsed;

				fpsCounts++;
			}

		}

		double getFramesPerSecond() const {

			return framesPerSecond;
		}

		double getMinimumFPS() const {

			return minimumFPS;
		}

		double getMaximumFPS() const {

			return maximumFPS;
		}

		gu_seconds getAverageFPS() const {

			return ((gu_seconds)averageFPS) / (gu_seconds)fpsCounts;
		}

		gu_seconds getSecondsPerFrame() const {

			return secondsPerFrame;
		}

		gu_seconds getMinimumSPF() const {

			return minimumSPF;
		}

		gu_seconds getMaximumSPF() const {

			return maximumSPF;
		}

		gu_seconds getAverageSPF() const {

			return averageSPF / (gu_seconds)fpsCounts;
		}

	};


public:

	// Clock status
	enum class clock_state : uint8_t { STOPPED = 0, RUNNING };

private:

	gu_time_index						baseTime = 0;
	gu_time_index						prevTimeIndex = 0;
	gu_time_index						currentTimeIndex = 0;

	gu_time_interval					deltaTime = 0;

	gu_time_index						stopTimeIndex = 0;
	gu_time_interval					totalStopTime = 0;

	clock_state							clockState = clock_state::STOPPED;

	std::unique_ptr<GUFrameCounter>		frameCounter = nullptr;

	std::string							clockName;


	//
	// Private interface
	//

	// Constructor - called internally by the CreateClock factory method defined above
	SMClock_actual(const std::string& newClockName, const clock_state initialClockState);


public:

	//
	// Public interface - Class methods
	//

	// Get the actual system clock time index
	static gu_time_index ActualTime();

	// Convert time interval measured in clock ticks into seconds (gu_seconds)
	static gu_seconds ConvertTimeIntervalToSeconds(gu_time_interval t);

	// Clock factory method
	static std::shared_ptr<SMClock_actual> CreateClock(const std::string& clockName, const clock_state initialClockState = clock_state::RUNNING);


	//
	// Public interface - Instance methods
	//

	// Update methods
	void start();
	void stop();
	void tick();
	void reset();

	// Query methods
	gu_seconds actualTimeElapsed() const;
	gu_seconds gameTimeElapsed() const;
	gu_seconds gameTimeDelta() const;

	// Return true if the clock is in the STOPPED state, otherwise return false
	bool clockStopped() const;

	void reportTimingData() const;

	double framesPerSecond() const;
	double minimumFPS() const;
	double maximumFPS() const;
	gu_seconds averageFPS() const;
	gu_seconds secondsPerFrame() const;
	gu_seconds minimumSPF() const;
	gu_seconds maximumSPF() const;
	gu_seconds averageSPF() const;
};

using SMClock = std::shared_ptr < SMClock_actual > ;
