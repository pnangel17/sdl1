
#include "Player.h"
#include "Sprite.h"


#pragma region Player state class implementations

void Player::MovingLeft::update(Player *player, KeyFlags keys, float timeDelta) {

	Point2f pos = player->getPos();
	float speed = player->getSpeed();
	float theta = player->getTheta();

	bool changeState = false;
	PlayerState newState;

	if (keys & Keys::Left) {

		pos.x -= speed * timeDelta;
	
	} else if (keys & Keys::Right) {

		pos.x += speed * timeDelta;

		// Handle state transition - don't update player here - set flag and update player at end of update function
		changeState = true;
		newState = PlayerState::MovingRight;
	}

	if (keys & Keys::Up) {

		pos.y -= speed * timeDelta;

		if (!(keys & (Keys::Left | Keys::Right))) {

			changeState = true;
			newState = PlayerState::MovingUp;
		}
	
	} else if (keys & Keys::Down) {

		pos.y += speed * timeDelta;

		if (!(keys & (Keys::Left | Keys::Right))) {

			changeState = true;
			newState = PlayerState::MovingDown;
		}
	}

	theta -= (360.0f / 60.0f) * timeDelta;


	// Update player
	player->setPos(pos);
	player->setTheta(theta);

	if (changeState == true) {

		onExit(player, timeDelta); // Call onExit for the current state
		player->setCurrentState(newState); // Do transition
		player->getBehaviour(newState)->onEntry(player, timeDelta); // Call onEntry for the new state
	}
}

void Player::MovingLeft::onEntry(Player *player, float timeDelta) {

	player->setTheta(0.0f);
}

void Player::MovingLeft::onExit(Player *player, float timeDelta) { /* Do nothing for now */ }


void Player::MovingRight::update(Player *player, KeyFlags keys, float timeDelta) {

	Point2f pos = player->getPos();
	float speed = player->getSpeed();
	float theta = player->getTheta();

	bool changeState = false;
	PlayerState newState;

	if (keys & Keys::Right) {

		pos.x += speed * timeDelta;
	
	} else if (keys & Keys::Left) {

		pos.x -= speed * timeDelta;

		changeState = true;
		newState = PlayerState::MovingLeft;
	}

	if (keys & Keys::Up) {

		pos.y -= speed * timeDelta;

		if (!(keys & (Keys::Left | Keys::Right))) {

			// Only interested in leaving MovingRight state if the right key is not held down
			changeState = true;
			newState = PlayerState::MovingUp;
		}
	
	} else if (keys & Keys::Down) {

		pos.y += speed * timeDelta;

		if (!(keys & (Keys::Left | Keys::Right))) {

			// Only interested in leaving MovingRight state if the right key is not held down
			changeState = true;
			newState = PlayerState::MovingDown;
		}
		
	}

	theta += (360.0f / 60.0f) * timeDelta;


	// Update player
	player->setPos(pos);
	player->setTheta(theta);

	if (changeState == true) {

		onExit(player, timeDelta); // Call onExit for the current state
		player->setCurrentState(newState); // Do transition
		player->getBehaviour(newState)->onEntry(player, timeDelta); // Call onEntry for the new state
	}
}

void Player::MovingRight::onEntry(Player *player, float timeDelta) {

	player->setTheta(0.0f);
}

void Player::MovingRight::onExit(Player *player, float timeDelta) { /* Do nothing for now */ }


void Player::MovingUp::update(Player *player, KeyFlags keys, float timeDelta) {

	Point2f pos = player->getPos();
	float speed = player->getSpeed();
	float theta = player->getTheta();

	bool changeState = false;
	PlayerState newState;

	if (keys & Keys::Up) {

		pos.y -= speed * timeDelta;
	
	} else if (keys & Keys::Down) {

		pos.y += speed * timeDelta;

		changeState = true;
		newState = PlayerState::MovingDown;
	}
	
	if (keys & Keys::Left) {

		pos.x -= speed * timeDelta;

		if (!(keys & (Keys::Up | Keys::Down))) {

			changeState = true;
			newState = PlayerState::MovingLeft;
		}

	} else if (keys & Keys::Right) {

		pos.x += speed * timeDelta;

		if (!(keys & (Keys::Up | Keys::Down))) {
			
			changeState = true;
			newState = PlayerState::MovingRight;
		}
	}

	theta += (360.0f / 60.0f) * 4.0f * timeDelta;

	// Update player
	player->setPos(pos);
	player->setTheta(theta);

	if (changeState == true) {

		onExit(player, timeDelta); // Call onExit for the current state
		player->setCurrentState(newState); // Do transition
		player->getBehaviour(newState)->onEntry(player, timeDelta); // Call onEntry for the new state
	}
};

void Player::MovingUp::onEntry(Player *player, float timeDelta) { /* Do nothing for now */ }

void Player::MovingUp::onExit(Player *player, float timeDelta) { /* Do nothing for now */ }


void Player::MovingDown::update(Player *player, KeyFlags keys, float timeDelta) {

	Point2f pos = player->getPos();
	float speed = player->getSpeed();
	float theta = player->getTheta();

	bool changeState = false;
	PlayerState newState;

	
	if (keys & Keys::Down) {

		pos.y += speed * timeDelta;
	
	} else if (keys & Keys::Up) {

		pos.y -= speed * timeDelta;

		changeState = true;
		newState = PlayerState::MovingUp;
	}

	if (keys & Keys::Left) {

		pos.x -= speed * timeDelta;

		if (!(keys & (Keys::Up | Keys::Down))) {

			changeState = true;
			newState = PlayerState::MovingLeft;
		}

	} else if (keys & Keys::Right) {

		pos.x += speed * timeDelta;

		if (!(keys & (Keys::Up | Keys::Down))) {

			changeState = true;
			newState = PlayerState::MovingRight;
		}
	}

	theta += (360.0f / 60.0f) * 8.0f * timeDelta;

	// Update player
	player->setPos(pos);
	player->setTheta(theta);

	if (changeState == true) {

		onExit(player, timeDelta); // Call onExit for the current state
		player->setCurrentState(newState); // Do transition
		player->getBehaviour(newState)->onEntry(player, timeDelta); // Call onEntry for the new state
	}
};

void Player::MovingDown::onEntry(Player *player, float timeDelta) { /* Do nothing for now */ }

void Player::MovingDown::onExit(Player *player, float timeDelta) { /* Do nothing for now */ }

#pragma endregion


//
// Implementation of Player methods
//

Player::Player(Point2f initialPosition, SDL_Renderer* renderer) {

	// Default the player to 
	pos = Point2f(initialPosition.x, initialPosition.y);

	theta = 0.0f;

	playerSprites[PlayerState::MovingLeft] = new Sprite("Assets\\Images\\player_left.png", renderer);
	playerSprites[PlayerState::MovingRight] = new Sprite("Assets\\Images\\player_right.png", renderer);
	playerSprites[PlayerState::MovingUp] = playerSprites[PlayerState::MovingLeft];
	playerSprites[PlayerState::MovingDown] = playerSprites[PlayerState::MovingLeft];

	// Create all state objects
	states[PlayerState::MovingLeft] = new Player::MovingLeft();
	states[PlayerState::MovingRight] = new Player::MovingRight();
	states[PlayerState::MovingUp] = new Player::MovingUp();
	states[PlayerState::MovingDown] = new Player::MovingDown();

	// Set initial state
	currentState = PlayerState::MovingLeft;
}


Player::~Player() {

	// Clean-up resources
	if (playerSprites[PlayerState::MovingLeft]) {

		delete playerSprites[PlayerState::MovingLeft];
	}

	if (playerSprites[PlayerState::MovingRight]) {

		delete playerSprites[PlayerState::MovingRight];
	}

	if (playerSprites[PlayerState::MovingUp]) {

		delete playerSprites[PlayerState::MovingUp];
	}

	if (playerSprites[PlayerState::MovingDown]) {

		delete playerSprites[PlayerState::MovingDown];
	}
}


#pragma region Accessor methods

Point2f Player::getPos() {

	return pos;
}

void Player::setPos(Point2f newPos) {

	pos = newPos;
}

float Player::getTheta() {

	return theta;
}

void Player::setTheta(float newTheta) {

	theta = newTheta;
}

float Player::getSpeed() {

	return speed;
}

void Player::setSpeed(float newSpeed) {

	speed = newSpeed;
}

PlayerState Player::getCurrentState() {

	return currentState;
}

void Player::setCurrentState(PlayerState newState) {

	currentState = newState;
}

Behaviour* Player::getBehaviour(PlayerState stateBehaviour) {

	return states[stateBehaviour];
}

#pragma endregion


void Player::update(KeyFlags keys, float timeDelta) {

	// Delegate update to current state (don't have to implement everything in one function as was the case with the uber-function approach
	states[currentState]->update(this, keys, timeDelta);
}


void Player::draw(SDL_Renderer* renderer) {

	playerSprites[currentState]->drawSprite(pos, renderer, 100, theta, SDL_RendererFlip::SDL_FLIP_NONE);
}
