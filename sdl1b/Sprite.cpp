
#include "Sprite.h"
#include <SDL_image.h>
#include "Point2f.h"


Sprite::Sprite(char *filename, SDL_Renderer *renderer) {

	SDL_Surface* surface;

	// Load image into SDL_Surface
	surface = IMG_Load(filename);

	// Create a texture object from the loaded image - we need the renderer we're going to use to draw this as well!
	texture = SDL_CreateTextureFromSurface(renderer, surface);

	// Setup source rectangle to cover the entire image
	sourceRectangle.x = 0;
	sourceRectangle.y = 0;
	SDL_QueryTexture(texture, 0, 0, &(sourceRectangle.w), &(sourceRectangle.h));

	// Clean-up - we're done with 'surface' now our texture has been created
	SDL_FreeSurface(surface);
}

Sprite::~Sprite() {

	if (texture) {

		SDL_DestroyTexture(texture);
	}
}

void Sprite::drawSprite(Point2f pos, SDL_Renderer *renderer, int targetWidth, float theta, SDL_RendererFlip flip) {

	SDL_Rect targetRect;

	// Derived value
	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = targetWidth; // player size on-screen

	float hwRatio = float(sourceRectangle.h) / float(sourceRectangle.w);
	targetRect.h = int(float(targetRect.w)  * hwRatio);

	//SDL_RenderCopy(renderer, playerTexture, &sourceRectangle, &targetRect);
	SDL_RenderCopyEx(renderer, texture, &sourceRectangle, &targetRect, theta, 0, flip);
}
