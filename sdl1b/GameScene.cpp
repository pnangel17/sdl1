
#include "GameScene.h"
#include <SDL.h>
#include "SMGame.h"
#include "Player.h"

GameScene::GameScene(SDL_Window* window, SDL_Renderer* renderer) : SMScene(window, renderer) {
	
	// Game specific initialisation
	player = new Player(Point2f(0.0f, 0.0f), getRenderer());

}

GameScene::~GameScene() {

	if (player)
		delete player;

}


// The main game loop...
uint32_t GameScene::run(void) {

	static uint32_t prevTimeIndex = SDL_GetTicks();

	while (quitGame==false) {

		// 1. Handle any waiting events
		handleEvents();


		// 2. Update game state

		// Calculate time elapsed
		uint32_t currentTimeIndex = SDL_GetTicks();
		uint32_t timeDelta = currentTimeIndex - prevTimeIndex;
		float timeDeltaInSeconds = float(timeDelta) / 1000.0f;

		// Updates / animation should be based on the time elapsed
		updateState(timeDeltaInSeconds);

		// Store current time index into prevTimeIndex for next frame
		prevTimeIndex = currentTimeIndex;


		// 3. Draw current game state
		render();
	
	}  // 4. Repeat

	
	return 0;
};


// Get next event of the event queue and process accordingly
void GameScene::handleEvents() {

	SDL_Event event;
	
	// Check for next event
	SDL_PollEvent(&event);

	switch (event.type) {

	// Check if window closed
	case SDL_QUIT:

		quitGame = true;
		break;

	// Key pressed event
	case SDL_KEYDOWN:

		// Toggle key states based on key pressed
		// Note: Don't XOR - if we do we'll toggle key flags on/off every event generated for a held-down key!
		switch (event.key.keysym.sym)
		{
		case SDLK_UP:
			keyState |= Keys::Up;
			break;

		case SDLK_DOWN:
			keyState |= Keys::Down;
			break;

		case SDLK_LEFT:
			keyState |= Keys::Left;
			break;

		case SDLK_RIGHT:
			keyState |= Keys::Right;
			break;

		case SDLK_ESCAPE:
			quitGame = true;
			break;
		}
		break;

	// Key released event
	case SDL_KEYUP:

		switch (event.key.keysym.sym)
		{
		case SDLK_UP:
			keyState &= (~Keys::Up);
			break;

		case SDLK_DOWN:
			keyState &= (~Keys::Down);
			break;

		case SDLK_LEFT:
			keyState &= (~Keys::Left);
			break;

		case SDLK_RIGHT:
			keyState &= (~Keys::Right);
			break;
		}

		break;
	}
}


// Update game state
void GameScene::updateState(float timeDelta) {

	// Update player with key states
	player->update(keyState, timeDelta);
}


// Draw current game state
void GameScene::render(void) {

	SDL_Renderer* renderer = getRenderer();

	// 1. Clear the screen
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
	SDL_RenderClear(renderer);

	// 2. Draw the player
	player->draw(renderer);

	// 3. Present the current frame to the screen
	SDL_RenderPresent(renderer);
}

