#pragma once

#include "SMScene.h"
#include "KeyState.h"


// Forward reference declarations for SMGame and Player classes.  We'll actually include the header files for these
// in the GameScene.cpp file - for now, all we need to know is that these classes exist in our project
class SMGame;
class Player;


//
// GameScene represents the main game we'll implement (we could have different scenes for different levels if we wanted to).
// This stores everything we need for the game.  The constructor is responsible for initialisation and the run() method
// contains the main game loop.
//

class GameScene : public SMScene {

private:

	// Flag to indicate when the game will quit - when it does we'll leave the run() method and exit the GameScene
	bool		quitGame = false;

	// Our main player
	Player*		player = nullptr;

	// Track which keys have been pressed
	KeyFlags	keyState = 0;


	// Private API - these methods are NOT called outside the GameScene object - so are declared here as private

	// For each update frame, we handle events such as keyboard and mouse input
	void handleEvents(void);

	// Update the game state - player, AI etc.
	void updateState(float timeDelta);

	// Render the current game state (the current frame of animation if you like)
	void render(void);
	

public:

	// Constructor - perform main game initialisation here.  Load images, sounds etc.
	GameScene(SDL_Window*	window, SDL_Renderer* renderer);

	// Destructor
	~GameScene();


	// The main game loop.  Once run() has been called, we stay in the game loop until the 'quitGame' flag is set to true.
	uint32_t run(void);
};