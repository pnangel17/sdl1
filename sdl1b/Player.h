#pragma once

#include <SDL.h>
#include "KeyState.h"
#include "Point2f.h"


class Sprite;
class Player;


// Base class for all state handling - every state class inherits from Behaviour and overrides the update method.  Behaviour is an abstract class.
class Behaviour {

public:

	virtual void update(Player *player, KeyFlags keys, float timeDelta) = 0;

	virtual void onEntry(Player *player, float timeDelta) = 0;
	virtual void onExit(Player *player, float timeDelta) = 0;
};


// Player states
enum PlayerState {

	MovingLeft = 0,
	MovingRight = 1,
	MovingUp = 2,
	MovingDown = 3
};


//
// Player models a simple interactive player
//

class Player {

	Sprite				*playerSprites[4] = { nullptr, nullptr, nullptr, nullptr };

	Point2f				pos; // Player's position in the world

	float				speed = 100.0f; // pixels to move per second

	float				theta; // Player's orientation

	Behaviour			*states[4] = { nullptr, nullptr, nullptr, nullptr };
	PlayerState			currentState;


	//
	// Nested classes to model Player's behaviour - don't need to be visible outside Player
	//

	class MovingLeft : public Behaviour {

		void update(Player *player, KeyFlags keys, float timeDelta);
		void onEntry(Player *player, float timeDelta);
		void onExit(Player *player, float timeDelta);
	};

	class MovingRight : public Behaviour {

		void update(Player *player, KeyFlags keys, float timeDelta);
		void onEntry(Player *player, float timeDelta);
		void onExit(Player *player, float timeDelta);
	};

	class MovingUp : public Behaviour {

		void update(Player *player, KeyFlags keys, float timeDelta);
		void onEntry(Player *player, float timeDelta);
		void onExit(Player *player, float timeDelta);
	};

	class MovingDown : public Behaviour {

		void update(Player *player, KeyFlags keys, float timeDelta);
		void onEntry(Player *player, float timeDelta);
		void onExit(Player *player, float timeDelta);
	};

public:

	// The Player constructor takes the initial position of the player, the path to the sprite image to use and a pointer to SDL's renderer (this is needed to load and create the sprite image!)
	Player(Point2f initialPosition, SDL_Renderer*);

	// Destructor - clean-up resources we don't need any more
	~Player();

	// Accessor methods

	Point2f getPos();
	void setPos(Point2f newPos);

	float getTheta();
	void setTheta(float newTheta);

	float getSpeed();
	void setSpeed(float newSpeed);

	PlayerState getCurrentState();
	void setCurrentState(PlayerState newState);

	Behaviour* getBehaviour(PlayerState stateBehaviour);

	// Update the player - we call this every iteration of the main game loop
	void update(KeyFlags keys, float timeDelta);

	// Draw the player - we call this every iteration of the main game loop
	void draw(SDL_Renderer* renderer);
};