#pragma once

#include <SDL.h>


class Point2f;


class Sprite {

private:

	SDL_Texture*		texture = nullptr; // Sprite image
	SDL_Rect			sourceRectangle; // Which part of the sprite image

public:

	Sprite(char *filename, SDL_Renderer *renderer);
	~Sprite();

	void drawSprite(Point2f pos, SDL_Renderer *renderer, int targetWidth, float theta, SDL_RendererFlip flip);
};